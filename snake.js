const snakeboard = document.getElementById("board");
const snakeboard_ctx = snakeboard.getContext("2d");
const boardBorder = 'black';
const boardColour = "white";
const snakeColour = 'green';
const snakeBorder = 'black';
    
const snake = [
  {x: 45, y: 15},
  {x: 30, y: 15},
  {x: 15, y: 15}      
  ]
   
let score = 0;
let food_x, food_y;   
let dx = 15, dy = 0;
    
genFood();
play();
document.addEventListener("keydown", changeDirection);
    
function play() {
  if (gameEnded()) {
    document.getElementById("status").innerHTML = "Game over";
    return;
  }
  setTimeout(function () {
    clearBoard();
    drawFood();
    moveSnake();
    drawSnake(); 
    play();
  }, 100)
}

function clearBoard() { 
  snakeboard_ctx.fillStyle = boardColour;     
  snakeboard_ctx.strokestyle = boardBorder;
  snakeboard_ctx.fillRect(0, 0, snakeboard.width, snakeboard.height);
  snakeboard_ctx.strokeRect(0, 0, snakeboard.width, snakeboard.height);
}

function randomFood(max) {
  return Math.round((Math.random() * max) / 15) * 15;
}

function genFood() {  
  food_x = randomFood(375);
  food_y = randomFood(375);      
  snake.forEach(function foodEaten(part) {
    const hasEaten = part.x == food_x && part.y == food_y;
    if (hasEaten) genFood();
  });
}

function drawFood() {
  snakeboard_ctx.fillStyle = 'green';
  snakeboard_ctx.strokestyle = 'black';
  snakeboard_ctx.fillRect(food_x, food_y, 15, 15);
  snakeboard_ctx.strokeRect(food_x, food_y, 15, 15);
}
    
function drawSnake() {
  snake.forEach(drawSnakePart)
}

function drawSnakePart(snakePart) {
  snakeboard_ctx.fillStyle = snakeColour;
  snakeboard_ctx.strokestyle = snakeBorder;
  snakeboard_ctx.fillRect(snakePart.x, snakePart.y, 15, 15);
  snakeboard_ctx.strokeRect(snakePart.x, snakePart.y, 15, 15);
}
 
function moveSnake() {
  const head = {x: snake[0].x + dx, y: snake[0].y + dy};
  snake.unshift(head);
  const foodEaten = snake[0].x === food_x && snake[0].y === food_y;
  if (foodEaten) {
    ++score;
    document.getElementById('score').innerHTML = score;
    genFood();
  } else {
    snake.pop();
  }
}
      
function gameEnded() {
  for (let i = 2; i < snake.length; i++) {
    if (snake[i].x === snake[0].x && snake[i].y === snake[0].y) return true
  }
  const hitLeftWall = snake[0].x <= 0;
  const hitRightWall = snake[0].x >= snakeboard.width - 15;
  const hitToptWall = snake[0].y <= 0;
  const hitBottomWall = snake[0].y >= snakeboard.height - 15;
  return hitLeftWall || hitRightWall || hitToptWall || hitBottomWall
}

function changeDirection(event) {
  const LEFT_KEY = 37;
  const RIGHT_KEY = 39;
  const UP_KEY = 38;
  const DOWN_KEY = 40;
  const keyPressed = event.keyCode;
  const goingUp = dy === -15;
  const goingDown = dy === 15;
  const goingRight = dx === 15;
  const goingLeft = dx === -15;
  if (keyPressed === LEFT_KEY && !goingRight) {
    dx = -15;
    dy = 0;
  }
  if (keyPressed === UP_KEY && !goingDown) {
    dx = 0;
    dy = -15;
  }
  if (keyPressed === RIGHT_KEY && !goingLeft) {
    dx = 15;
    dy = 0;
  }
  if (keyPressed === DOWN_KEY && !goingUp) {
    dx = 0;
    dy = 15;
  }
}
